package com.shyam.flightreservation.entities;

import javax.persistence.Entity;

@Entity
public class Passenger extends AbstractEntity {

	private String firstName;
	private String lastName;
	private String middletName;
	private String email;
	private String phone;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddletName() {
		return middletName;
	}

	public void setMiddletName(String middletName) {
		this.middletName = middletName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
