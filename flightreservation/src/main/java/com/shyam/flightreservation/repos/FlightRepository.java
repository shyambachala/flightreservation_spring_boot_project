package com.shyam.flightreservation.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shyam.flightreservation.entities.Flight;

public interface FlightRepository extends JpaRepository<Flight, Long> {

}
