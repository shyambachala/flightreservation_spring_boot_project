package com.shyam.flightreservation.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shyam.flightreservation.entities.Passenger;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {

}
